#!/bin/bash
set -e
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################


package="cnijfilter-mg3500"

#----------------------------------------------------------------------------------

#checking which helper is installed
if pacman -Qi yaourt &> /dev/null; then

	echo "Installing with yaourt"
	yaourt -S --noconfirm $package

elif pacman -Qi pacaur &> /dev/null; then

	echo "Installing with pacaur"
	pacaur -S --noconfirm --noedit  $package

elif pacman -Qi packer &> /dev/null; then

	echo "Installing with packer"
	packer -S --noconfirm --noedit  $package

fi






program="scangearmp-mg3500"

if which pacaur > /dev/null; then

	echo "Installing with pacaur"
	pacaur -S --noconfirm --noedit  $program

elif which packer > /dev/null; then

	echo "Installing with packer"
	packer -S --noconfirm --noedit  $program

elif which yaourt > /dev/null; then

	echo "Installing with yaourt"
	yaourt -S --noconfirm $program

fi



############################################################################

echo "################################################################"
echo "###################    canon installed    ######################"
echo "################################################################"
